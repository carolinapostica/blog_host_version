//Tie login button with login modal

$("#loginButton").click(function() {
    $("#loginModal").modal("show")
});
//Tie registration button with registration modal

$("#registerButton").click(function() {
    $("#registerModal").modal("show")
});

//CHange value on collapse button read more
$('.collapse-button').click(function() {
    $(this).text(function(i, old) {
        return old == 'read more...' ? 'show less' : 'read more...';
    });
});
// $("#loginButton").click(function() {
//     $(this).text(function(i, old) {
//         return old == 'LOG IN' ? 'LOG OUT' : 'LOG IN';
//     });
// });

$('#userPostsArchive').click(function() {
    $(this).text(function(i, old) {
        return old == 'CLICK HERE TO SEE ALL YOUR POSTS!' ? 'BACK TO MAIN ARTICLES' : 'CLICK HERE TO SEE ALL YOUR POSTS!';
    });
    $('#readNews').text(function(i, old) {
        return old == 'Main articles' ? 'Your articles' : 'Main articles';
    });
});



$(document).ready(function() {
    // Add scrollspy to <body>
    $('body').scrollspy({
        target: "#readNews",
        offset: 50
    });

    // Add smooth scrolling on all links inside the navbar
    $("#discoverButton a").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});



// POST SERVER
